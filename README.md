# Simple Free Way To Manually Label Objects In Each Frame Of A Video
- Date: 9-18-2021

## Requirements:
- Ubuntu  : https://ubuntu.com/download
- ffmpeg  : sudo apt install ffmpeg
- labelme : 
	- Method 1:
		- cmd: sudo apt install labelme
	- Method 2:
		- cmd: pip3 install labelme

## LabelMe Repo:
- Link: https://github.com/wkentaro/labelme

## How To Use LabelMe:
- Tutorial: https://www.youtube.com/watch?v=oTcYveHI07w

## How To Setup:
- 0) Make sure you are using the Ubuntu OS and you install/setup all the requirements listed in this README.
- 1) Make sure to know how to use LabelMe, checkout the tutorial above.
- 2) Git clone the repo to your desired directory.
- 3) Move your video to the git repo you just cloned.
- 4) Run the following command:
		- cmd: bash setup.sh [video_name/path] [video's fps]
		- about:
			- the setup.sh script gets a video and converts it into images/frames. it also moves files and setups directories for the main process.
			- to really use this tool correctly, you need to know the fps your video was captured in. 
			- here is an example of the cmd: bash setup.sh test.webm 30
- 5) After Step #4, the following directories are created: 
		- images/
		- labels/
- 6) When Steps #1-#5 are completed, run the following command: labelme
		- NOTE: this will launch the labelme labeling tool
- 7) With labelme opened now, following the LabelMe tutorial listed to setup everything else for this environment and start labeling.
- 8) After everything in Step #7 and after all the labeling is done, your labels should be stored in the labels/ directory. These labels are stored as json files named after the frame they are based off of. You can process theses labels and their data by making a Python script, but that is up to you as to how you implment and use such a script.

## How To "Reset" Setup:
- After you run setup.sh, you SHOULD NOT run it again. Knowing that, if you want to run it again, either clone the repo again in a different directory or you can just remove the images/ & labels/ directory and the video file itself, then follow the "How To Setup" steps again.

## Sources/Credits:
- https://gist.github.com/loretoparisi/a9277b2eb4425809066c380fed395ab3
- https://www.youtube.com/watch?v=oTcYveHI07w
- https://github.com/wkentaro/labelme


