# Title: SetUp Script For labelme-helpme Repo
# Date:  9-18-2021

# convert inputed video and fps from a video to png-images
ffmpeg -i $1 -vf fps=$2 frame%d.png

# create & move all frames/png(s) to images/ directory
mkdir images
mv *.png images

# create labels/ directory
mkdir labels

# print some quick information
echo
echo "-Space: " $(du -hs * | sort -hr | grep "images")
echo "-Video:  "$1
echo "-Frames: "$(ls images/ | wc -l)
echo "-Start:  labelme"

